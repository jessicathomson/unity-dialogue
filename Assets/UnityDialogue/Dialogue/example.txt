
block start:
	# This is a comment
	var test = "t\#hing"  # Another comment
	print {test}
	if test == "t\#hing":
		print "Test worked"
	say "This is a thing???"
	say "here is more stuff " +
	"which is across multiple lines"

	run TestType false# thing
	if testThing:
		run TestType {testThing}
	choice:
		"Choice 1":
			var testVar = 1
			jump test
		"Choice 2":
			var testVar = 2
			jump test
		"Choice 3":
			var testVar = 3
			jump test
		"Choice 4":
			var testVar = 4
			jump test
endblock


block test:
	say "You chose choice {testVar}!"
	say "This is another thing!"
	say "Whoa this works, that's pretty cool!"
	say "I can also have multiple choices in a script"
	choice:
		"Second Choice 1":
			jump end
		"Second Choice 2":
			jump end
		"Second Choice 3":
			jump end
endblock


block end:
	say "See, I told you!"
	say "You may have also noted that I had an odd number of choices"
endblock