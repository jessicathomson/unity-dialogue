﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Reflection;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;

[RequireComponent(typeof(Interpreter))]
[RequireComponent(typeof(Button))]
public class Dialogue : MonoBehaviour
{

    //TODO: Saving
    //TODO: Set sizing of DialogueText on init (with buffer)
    //TODO: Default placement for speaker

    public delegate void VoidFunc();

    public GameObject buttonPrefab;

    // Add elements to this (through code or the editor) which you wish
    //  to be toggled with the dialogue. 
    /***  NOTE: All Images and Text under Dialogue are automatically added to this ***/
    private List<MaskableGraphic> m_ToggleComponents = new List<MaskableGraphic>();

    private Interpreter _curInterpreter;
    protected Interpreter CurInterpreter
    { get { return _curInterpreter ? _curInterpreter : GetComponent<Interpreter>(); } }

    // How many choices will fit on the screen?
    public int choiceWidth  = 2;
    public int choiceHeight = 2;

    // Either set in editor for single dialogue games, or through script for multi
    public string saveName = "";

    // Set in constructor; these are the elements of the default DialogueManager object
    protected Text m_Text;
    protected Image m_BgImage;
    protected Button m_Button;

    //protected Text m_SpeakerText;
    //protected Image m_SpeakerBgImage;

    // Error logging may need to be turned off for certain override behavior
    protected bool logErrors = true;

    /*
     * Add any Components to this list to be toggled when
     * the Dialogue is activated / deactivated  
     */
    protected List<GameObject> m_ChoiceButtons = new List<GameObject>();

    private RectTransform m_Background = null;

    protected MaskableGraphic[] m_VisibleElements;
    public static Dialogue Current;

    // Use this for initialization
    protected virtual void Start ()
    {
        Current = this;
        
        //Checks for missing components
        {
            if (transform.Find("Background"))
                m_Background = transform.Find("Background").GetComponent<RectTransform>();
            else if(logErrors)
                Debug.LogError("\"Background\" element not found under DialogueManager");
            if (logErrors && !m_Background.Find("DialogueText"))
                Debug.LogError("\"DialogueText\" element not found under Background");
            if (m_Background.Find("DialogueText") && m_Background.Find("DialogueText").GetComponent<Text>())
                m_Text = transform.Find("Background").Find("DialogueText").GetComponent<Text>();
            else if(logErrors)
                Debug.LogError("Text component not found on \"DialogueText\" object");

            m_BgImage = transform.Find("Background").GetComponent<Image>();
            m_Button = GetComponent<Button>();

            //ToggleVisibleElements();

            //m_SpeakerBgImage = transform.FindChild("SpeakerLabel").GetComponent<Image>();
            //m_SpeakerText = m_SpeakerBgImage.gameObject.transform.FindChild("SpeakerName").GetComponent<Text>();

            //SetSpeaker("null");
        }
        
        CurInterpreter.Initialize();

        /*************************************************************
         *  Uncomment this to start the game with the dialogue open  *
         *         Otherwise, manually call it when needed           *
         *************************************************************/
        StartUsing();
    }

    // Override if some child elements of DialogueManager should always be shown
    public virtual void ToggleVisibleElements(bool? b = null)
    {
        if (m_VisibleElements == null || m_VisibleElements.Length == 0)
            InitVisibleElements();

        // Debug.Log(m_VisibleElements.Length);

        if (b == true || b == false)
            foreach (MaskableGraphic r in m_VisibleElements)
                r.enabled = (bool)b;
        else
            foreach (MaskableGraphic r in m_VisibleElements)
                r.enabled = !r.enabled;

    }

    private void InitVisibleElements()
    {
        m_VisibleElements = GetComponentsInChildren<MaskableGraphic>();
    }

    public bool RunFunction(string functionName, string[] arguments)
    {
        List<object> argList = new List<object>();

        List<Type> argTypes = new List<Type>();

        foreach(string arg in arguments)
        {
            object val;
            argTypes.Add(GetConverted(arg, out val));
            argList.Add(val);
        }
        
        //dynamic c = Activator.CreateInstance(type);
        //args contains the parameters(only string type)
        //MethodInfo method = GetType().GetMethod(functionName, argTypes.ToArray());
        if (true)//if (method != null)
        {
            object ret = GetType().InvokeMember(functionName, BindingFlags.InvokeMethod, null, this, argList.ToArray());
            //object ret = method.Invoke(this, argList.ToArray());
            if (ret is bool && !(bool)ret)
                return false;
        }
        else
        {
            string sArgsTypes = "";
            foreach (Type type in argTypes)
                sArgsTypes += " " + type.ToString();
            Debug.LogError("Function " + functionName + " with argument types\n" + sArgsTypes + " not found");
        }

        return true;
    }

    private Type GetConverted(string arg, out object value)
    {
        float tmp;
        if (float.TryParse(arg, out tmp))
        {
            value = tmp;
            return typeof(float);
        }

        if (arg == "true" || arg == "false")
        {
            value = arg == "true";
            return typeof(bool);
        }

        value = arg;
        return typeof(string);
    }

    public virtual void StartUsing()
    {
        // Makes renderComponents visible
        ToggleVisibleElements(true);

        // Starts interpreter
        CurInterpreter.Execute();
    }

    public virtual void OnPress()
    {
        print(CurInterpreter);
        CurInterpreter.Execute();
    }

    internal virtual void HandleSay(string speakerName, string statement)
    {
        m_Text.text = statement;
    }
    
    internal virtual void HandleChoice(string choicePrompt, List<string> choiceTexts)
    {
        // Clears the current text
        m_Text.text = choicePrompt;
        SetInteractive(false);

        // Creates, sizes, and adds the elements
        float buffer = m_Background.rect.width * 0.02f;
        for (int i = 0; i < choiceTexts.Count; i++)
        {
            GameObject choiceButton = Instantiate(buttonPrefab);
            m_ChoiceButtons.Add(choiceButton);
            m_ChoiceButtons[m_ChoiceButtons.Count - 1].transform.SetParent(m_Background);

            RectTransform choiceRectTransform = choiceButton.GetComponent<RectTransform>();
            choiceRectTransform.sizeDelta = new Vector2((m_Background.rect.width / choiceWidth - buffer) * (transform.lossyScale.x), (m_Background.rect.height / choiceHeight - buffer) * (transform.lossyScale.y));
            choiceRectTransform.anchoredPosition = new Vector2(
                (m_Background.rect.width / choiceWidth) * (i % choiceWidth - 0.5f),
                ((1 - m_Background.rect.height / choiceHeight) * (i / choiceHeight - 0.5f)));
            choiceButton.transform.Find("Text").gameObject.GetComponent<Text>().text = choiceTexts[i];
            
            choiceButton.GetComponent<Button>().onClick.AddListener(delegate () { ButtonCallback(choiceButton); });
        }

        //TODO: Add / remove scrollRect script as needed
    }

    public virtual void SetInteractive(bool v)
    {
        m_Button.interactable = v;
    }

    protected void ClearButtons()
    {
        foreach (GameObject g in m_ChoiceButtons)
            Destroy(g);
        m_ChoiceButtons.Clear();
        m_Button.interactable = true;

        CurInterpreter.Execute();
        SetInteractive(true);
    }

    protected virtual void ButtonCallback(GameObject button)
    {
        //int i = m_ChoiceButtons.IndexOf(button) + 1;
        string choiceText = button.transform.Find("Text").GetComponent<Text>().text;
        CurInterpreter.Choose(choiceText);

        ClearButtons();
    }

    public virtual void StopUsing()
    {
        foreach (Behaviour b in m_ToggleComponents)
            b.enabled = false;
        foreach (GameObject g in m_ChoiceButtons)
            Destroy(g);

        ToggleVisibleElements(false);
    }
    
    public virtual void SaveToFile(string fileName = "")
    {
        CurInterpreter.Save(fileName);
    }

    // Use this for saving multiple characters' states
    // -- NOTE: I'd personally use a SerializableDictionary<string, Interpreter.InterpreterState>, with the string being the character's name / ID --
    public object GetSaveState()
    {
        return CurInterpreter.SaveData;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Dialogue))]
public class DialogueEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Dialogue dialogue = (Dialogue)target;
        if (GUILayout.Button("Toggle Visible Components"))
            dialogue.ToggleVisibleElements();
        if (GUILayout.Button("Delete Save File"))
        {
            string filePath = Application.persistentDataPath + "/" + Interpreter.SAVE_PREFIX + dialogue.saveName + ".dat";
            Debug.Log("File exists? " + File.Exists(filePath));
            File.Delete(filePath);
        }
    }
}
#endif