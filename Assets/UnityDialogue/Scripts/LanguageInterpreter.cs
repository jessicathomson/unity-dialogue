﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class LanguageInterpreter : Interpreter
{
    [HideInInspector] public new const string SAVE_PREFIX = "dialogueSave";
    /***************************************************************
     *  Generally speaking, you shouldn't need anything from here  *
     *  to add functionality, but feel free to look through if     *
     *  you want to know how it all works.                         *
     ***************************************************************/

    //TODO: Skip to endblock + 1 on fallthrough

    public TextAsset scriptFile;

    public bool saveOnQuit = false;

    public static string SaveName = "";

    protected Dialogue m_Dialogue;

    protected List<string> m_textLines = new List<string>();

    protected InterpreterState curState;
    public override object SaveData { get { return curState; } }

    //protected SerializableDictionary<string, bool> curState.BoolDict = new SerializableDictionary<string, bool>();
    //protected SerializableDictionary<string, string> curState.StringDict = new SerializableDictionary<string, string>();
    //protected SerializableDictionary<string, float> curState.FloatDict = new SerializableDictionary<string, float>();
    [HideInInspector]
    public bool shouldBreak = false;
    public bool shouldLoad = false;

    //private static SerializableDictionary<string, InterpreterState> bookmarks = new SerializableDictionary<string, InterpreterState>();

    #region Setup Initialization and public functionality

    public override void Initialize()
    {
        m_Dialogue = GetComponent<Dialogue>();

        SplitText();

        // Loads Interpreter state from file
        // TODO: Resumes previously left off location 
        if (shouldLoad)
            Load();
        else
            curState = new InterpreterState();
    }

    public void Pause()
    {
        EndScript();
        Save();
    }

    #endregion

    private void EndScript()
    {
        // Moves forward 1 line, to prevent repeating
        //curState.LineNum++;
        m_Dialogue.StopUsing();
    }

    // Executes until a pause / break is triggered
    public override void Execute()
    {
        while (ReadCurLine()) { };
    }

    protected string FirstWord(string line)
    {
        line = ClearTabs(line);
        return line.IndexOf(" ") != -1 ? line.Substring(0, line.IndexOf(" ")) : line;
    }

    private string FormatChoice(string choice)
    {
        List<string> args = new List<string>();
        FormatChoice(choice, out args);
        return args[0];
    }

    private string FormatChoice(string choice, out List<string> args)
    {
        args = new List<string>();
        if (choice[choice.Length - 1] == ' ')
            while (choice.Length > 0 && choice[choice.Length - 1] == ' ')
                choice = choice.Substring(0, choice.Length - 1);

        while (choice.Length > 0 && choice[0] == '\t')
            choice = choice.Substring(1);
        if (choice.LastIndexOf(":") == choice.Length - 1)
            choice = choice.Substring(0, choice.Length - 1);
        else
        {
            args.Add("");
            return "";
        }

        while (choice.Length > 0)
        {
            //Handles strings surrounded by quotes
            if (choice.IndexOf("\"") == 0)
            {
                choice = choice.Substring(1);
                args.Add(choice.Substring(0, choice.IndexOf("\"")));
                choice = choice.Substring(choice.IndexOf("\"", 1) + 1);
                //Clears spaces between arguments
                if (choice.Length > 0 && choice.Substring(0, 1) == " ")
                    choice = choice.Substring(1);
                continue;
            }
        }

        return args[0];
    }

    public object GetValue(string varName)
    {
        if (curState.StringDict.ContainsKey(varName)) return curState.StringDict[varName];
        if (curState.FloatDict.ContainsKey(varName)) return curState.FloatDict[varName];
        return curState.BoolDict.ContainsKey(varName) ? curState.BoolDict[varName] : false;
    }

    public void SetValue(string varName, bool value)
    { curState.BoolDict[varName] = value; }
    public void SetValue(string varName, float value)
    { curState.FloatDict[varName] = value; }
    public void SetValue(string varName, string value)
    { curState.StringDict[varName] = value; }

    #region Bools used for verifying current script context

    private bool InChoice()
    {
        int i = curState.LineNum;
        int curIndent = IndentLevel(m_textLines[i]);

        while (IndentLevel(m_textLines[i]) == curIndent)
            if (--i < 0) return false;

        return FirstWord(m_textLines[i]).Substring(0, 1) == "\"";
    }

    private bool InIf()
    {
        int i = curState.LineNum;
        int curIndent = IndentLevel(m_textLines[curState.LineNum]);
        while (IndentLevel(m_textLines[i]) == curIndent)
        {
            i--;
            if (i < 0)
                return false;
        }

        return FirstWord(m_textLines[i]) == "if";
    }

    #endregion

    #region Line by line script reading

    public bool ReadCurLine()
    {
        if (shouldBreak)
        {
            shouldBreak = false;
            return false;
        }

        // Break early if out of out of lines
        if (curState.LineNum >= m_textLines.Count - 1)
        {
            Debug.Log("Out of lines");
            EndScript();
            return false;
        }

        string lineText = m_textLines[curState.LineNum];

        if (lineText.IndexOf("{") != -1 && lineText.IndexOf("}") != -1)
            lineText = ReplaceVars(lineText);

        string fWord = FirstWord(lineText);
        if (fWord[0] == '\"') // Breaks to next line; fix for saving on multiline comment
        {
            curState.LineNum--;
            return true;
        }
        if (fWord[fWord.Length - 1] == ':')
            fWord = fWord.Substring(0, fWord.Length - 1);

        bool tryForNext = true;

        switch (fWord)
        {
            case "":
                break;
            case "block":
                curState.LineNum++;
                return true;
            case "if":
                return HandleIfStatement(lineText.Substring(lineText.IndexOf(" ") + 1));
            case "jump":
                string clearedTabs = ClearTabs(lineText);
                Jump(clearedTabs.Substring(clearedTabs.IndexOf(" ") + 1, clearedTabs.Length - 5));
                return true;
            case "say":
                HandleSayStatement(lineText.Substring(lineText.IndexOf(" ") + 1));
                tryForNext = false;
                break;
            case "sayToggle":
                HandleSayStatement(lineText.Substring(lineText.IndexOf(" ") + 1));
                tryForNext = true;
                break;
            case "trace":
            case "print":
            case "cout":
            case "echo":
            case "Log":
            case "Debug.Log":
                Debug.Log(lineText.Substring(lineText.IndexOf(" ") + 1));
                break;
            case "function":
            case "fun":
            case "run":
                if (HandleFunction(lineText.Substring(lineText.IndexOf(" ") + 1)) == false)
                {
                    curState.LineNum++;
                    return false;
                }
                break;
            case "end":
            case "endscript":
            case "endblock":
                EndScript();
                return false;
            case "choice":
                HandleChoiceStatement();
                curState.LineNum++;
                return false;
            case "var":
                HandleVarStatement(m_textLines[curState.LineNum]);
                break;
            default:
                Debug.LogError("Command " + fWord + " not found");
                break;
        }

        return NextLine(tryForNext);
    }

    private bool NextLine(bool tryForNext)
    {
        int curIndent = IndentLevel(m_textLines[curState.LineNum]);

        if (curIndent == IndentLevel(m_textLines[curState.LineNum + 1]))
        {
            curState.LineNum++;
            return tryForNext;
        }

        if (InChoice() && curIndent > IndentLevel(m_textLines[curState.LineNum + 1]))
        {
            int i = curState.LineNum;
            while (curIndent - 1 > IndentLevel(m_textLines[i]))
                i--;
            return curIndent - IndentLevel(m_textLines[i]) == 2;
        }

        if (InIf() && curIndent > IndentLevel(m_textLines[curState.LineNum + 1]))
        {
            curState.LineNum++;
            return tryForNext;
        }

        if (curIndent < IndentLevel(m_textLines[curState.LineNum + 1]) && FirstWord(m_textLines[curState.LineNum + 1]) != "endblock")
            Debug.LogError("Unknown indentation used on line " + (curState.LineNum + 1) + ", " + m_textLines[curState.LineNum + 1]);

        EndScript();
        return false;
    }

    private int PrevLine(int baseLine)
    {
        int tmpLine = baseLine == -1 ? curState.LineNum : baseLine;
        while (tmpLine-- > 0 && FirstWord(m_textLines[tmpLine]) != "block")
        {
            string fWord = FirstWord(m_textLines[tmpLine]);
            if (fWord == "block")
                return ++tmpLine;

            if (m_textLines[tmpLine].IndexOf("run startScene") != -1)
            {
                return tmpLine + 1;
            }

            if (fWord == "say" || fWord == "choice")
                return tmpLine;
        }

        return 0;
    }

    #endregion

    #region Powerhouse functions, handle specific calls from script

    internal override void Choose(string choiceText)
    {
        curState.LineNum--;
        int curIndent = IndentLevel(m_textLines[curState.LineNum]);
        int tempCurLine = curState.LineNum + 1;

        while (curIndent < IndentLevel(m_textLines[tempCurLine]))
        {
            if (FormatChoice(m_textLines[tempCurLine]) == choiceText)
            {
                curState.LineNum = ++tempCurLine;
                return;
            }
            tempCurLine++;
        }
        Debug.LogError("Choice \"" + choiceText + "\" not found");
    }

    protected bool HandleFunction(string statement)
    {
        List<string> args = new List<string>();

        // Parses out function name and statement, handling 0 parameter cases
        int spaceIndex = statement.IndexOf(" ");
        string functionName = spaceIndex != -1 ? statement.Substring(0, spaceIndex) : statement;
        statement = spaceIndex != -1 ? statement.Substring(statement.IndexOf(" ") + 1) : "";

        // --Parse out the values for function
        while (statement.Length > 0)
        {
            //Handles strings surrounded by quotes
            if (statement.IndexOf("\"") == 0)
            {
                statement = statement.Substring(1);
                args.Add(statement.Substring(0, statement.IndexOf("\"")));
                statement = statement.Substring(statement.IndexOf("\"", 1) + 1);
                if (statement.Length > 0 && statement.Substring(0, 1) == " ") //Clears spaces between arguments
                    statement = statement.Substring(1);
                continue;
            }
            //Handles standard vars / nums
            else if (statement.IndexOf(" ") == -1)
            {
                args.Add(statement);
                statement = "";
            }
            else
            {
                args.Add(statement.Substring(0, statement.IndexOf(" ")));
                statement = statement.Substring(statement.IndexOf(" ") + 1);
            }
        }

        return m_Dialogue.RunFunction(functionName, args.ToArray());
    }

    private void HandleVarStatement(string statement)
    {
        statement = statement.Substring(statement.IndexOf(" ") + 1);
        string varName = statement.Substring(0, statement.IndexOf(" "));
        statement = statement.Substring(statement.IndexOf(" ") + 1);
        string sign = statement.Substring(0, statement.IndexOf(" "));
        string value = statement.Substring(statement.IndexOf(sign) + 2);
        value = TrimQuotes(value);

        float parsedVal;
        switch (sign)
        {
            case "=":
            case "equals":
            case "==":
                if (value == "true" || value == "false")
                    curState.BoolDict[varName] = value == "true" ? true : false;
                else
                {
                    if (float.TryParse(value, out parsedVal))
                        curState.FloatDict[varName] = parsedVal;
                    else
                        curState.StringDict[varName] = value;
                }
                break;
            case "+=":
                if (float.TryParse(value, out parsedVal))
                    curState.FloatDict[varName] += parsedVal;
                else
                    curState.StringDict[varName] = value;
                break;
            case "-=":
                if (float.TryParse(value, out parsedVal))
                    curState.FloatDict[varName] -= parsedVal;
                else
                    Debug.LogError("Cannot apply -= to bool or string");
                break;
            default:
                break;
        }
    }
    private void HandleChoiceStatement()
    {
        int choiceIndent = IndentLevel(m_textLines[curState.LineNum]);
        List<string> choices = new List<string>();

        // Separates prompt from the choice line, if there is one
        string choicePrompt = m_textLines[curState.LineNum];
        int firstIndex = choicePrompt.IndexOf("choice") + "choice".Length;

        choicePrompt = choicePrompt.Substring(
            firstIndex,
            choicePrompt.IndexOf(":") - firstIndex
        );

        // Parses out the individual choice lines
        int lineNum = curState.LineNum + 1;
        while (IndentLevel(m_textLines[lineNum]) > choiceIndent)
        {
            if (IndentLevel(m_textLines[lineNum]) - 1 == choiceIndent)
            {
                List<string> argsList;
                FormatChoice(m_textLines[lineNum], out argsList);

                choices.Add(argsList[0]);
            }
            lineNum++;
        }

        m_Dialogue.HandleChoice(TrimQuotes(choicePrompt), choices);
    }

    private void HandleSayStatement(string v)
    {
        while ((m_textLines[curState.LineNum] as string)
            .Substring(m_textLines[curState.LineNum].Length - 1, 1) == "\\"
        || (m_textLines[curState.LineNum] as string)
            .Substring(m_textLines[curState.LineNum].Length - 1, 1) == "+")
        {
            v = v.Substring(0, v.LastIndexOf("\""));
            v += ClearTabs(m_textLines[++curState.LineNum]);
        }

        m_Dialogue.HandleSay("", TrimQuotes(v));
    }

    private bool HandleIfStatement(string statement) //[X]
    {
        bool result = false;

        statement = statement.Substring(statement.Length - 1, 1) == ":"
            ? statement.Substring(0, statement.Length - 1) : statement;

        if (statement.IndexOf(" ") == -1) //Bare boolean check [√]
        {
            result = statement.IndexOf("!") != -1
                ? !(bool)GetValue(statement.Substring(1)) : (bool)GetValue(statement);
        }
        else //Equality / greater than / less than
        {
            string varName = statement.Substring(0, statement.IndexOf(" "));
            statement = statement.Substring(statement.IndexOf(" ") + 1);
            string sign = statement.Substring(0, statement.IndexOf(" "));
            string value = statement.Substring(sign.Length + 1);

            float floatVal;
            bool isFloat = float.TryParse(value, out floatVal);
            if (sign != "==" && sign != "=" && value != "is" && value != "equals" && !isFloat)
                Debug.LogError("Comparison value for non-equality must be a number");
            value = TrimQuotes(value);

            switch (sign)
            {
                case "<=":
                    result = (float)GetValue(varName) <= floatVal;
                    break;
                case "<":
                    result = (float)GetValue(varName) < floatVal;
                    break;
                case ">=":
                    result = (float)GetValue(varName) >= floatVal;
                    break;
                case ">":
                    result = (float)GetValue(varName) > floatVal;
                    break;
                case "==":
                case "=":
                case "is":
                case "equals":
                    if (!isFloat && (value == "true" || value == "false"))
                        result = ((bool)GetValue(varName) == (value == "true"));
                    else if (isFloat)
                        result = (float)GetValue(varName) == floatVal;
                    else
                        result = (string)GetValue(varName) == value;
                    break;
            }

        }

        if (result)
            curState.LineNum++;
        else
        {
            int originalIndentLevel = IndentLevel(m_textLines[curState.LineNum]);
            while (IndentLevel(m_textLines[++curState.LineNum]) > originalIndentLevel) { }
            if (FirstWord(m_textLines[curState.LineNum]) == "endblock" || IndentLevel(m_textLines[curState.LineNum]) == 0)
                return false;
        }

        return true;
    }

    /***********************************************************
     * "'GoTo considered harmful', so I named it Jump instead" *
     *     -Ren'Py Source Code                                 *
     ***********************************************************/
    private void Jump(string blockName)
    {
        for (int i = 0; i < m_textLines.Count; i++)
        {
            if (m_textLines[i].Substring(0, 5) == "block"
                && m_textLines[i].IndexOf(":") != -1
                && m_textLines[i].Substring(6, m_textLines[i].Length - 7) == blockName)
            {
                curState.LineNum = i;
                return;
            }
        }
        Debug.LogError("Block " + blockName + " not found");
    }

    #endregion

    #region Parsing functions

    private string ReplaceVars(string lineText)
    {
        while (lineText.IndexOf("{") != -1 && lineText.IndexOf("}") != -1)
        {
            string formerText = lineText.Substring(0, lineText.IndexOf("{"));
            lineText = lineText.Substring(lineText.IndexOf("{") + 1);
            string varName = lineText.Substring(0, lineText.IndexOf("}"));
            if (GetValue(varName) is bool && !curState.BoolDict.ContainsKey(varName))
                Debug.Log("Var " + varName + " does not exist");
            lineText = lineText.Substring(lineText.IndexOf("}") + 1);
            lineText = formerText + GetValue(varName) + lineText;
        }
        return lineText;
    }

    // TODO: handle multiline comments as single line for saving
    private void SplitText()
    {
        string[] tempArr = scriptFile.text.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
        
        foreach (string text in tempArr)
        {

            string str = text;
            if (text == "" || ClearTabs(text) == "" || FirstWord(text)[0] == '#')
                continue;

            // Clears comments, ignoring escaped hashes
            while (str.LastIndexOf("#") != -1 && (str[str.LastIndexOf("#") - 1] != '\\'))
            {
                str = str.Substring(0, str.LastIndexOf("#"));
                while (str[str.Length - 1] == ' ')
                    str = str.Substring(0, str.Length - 1);
            }
            while (str.IndexOf("\\#") != -1)
            {
                str = str.Substring(0, str.IndexOf("\\#"))
                    + (str.IndexOf("\\#") != str.Length ? str.Substring(str.IndexOf("\\#") + 1) : "");
            }

            m_textLines.Add(str);
        }

    }

    private string TrimQuotes(string value)
    {
        while (value.IndexOf("\"") != -1)
            value = value.Substring(0, value.IndexOf("\""))
                      + value.Substring(value.IndexOf("\"") + 1);
        return value;
    }

    private int IndentLevel(string line)
    {
        for (int i = 0; i < line.Length; i++)
            if (line[i] != '\t')
                return i;
        return 0;
    }

    protected string ClearTabs(string line)
    {
        if (line != "")
        {
            while (line.Length > 0 && line[0] == '\t')
                line = line.Substring(1);
        }
        return line;
    }

    #endregion

    #region Save state handling

    private void OnApplicationQuit()
    {
        if (saveOnQuit)
            Save();
    }

    public override void Save(string saveName = "")
    {
        BinaryFormatter bf = new BinaryFormatter();
        //FileStream file = File.Open(Application.persistentDataPath + "/dialogueSave" + saveName + ".dat", FileMode.OpenOrCreate);

        //if (file.Length > 0)
        //    curState = bf.Deserialize(file) as InterpreterState;
        //else
        //    curState = new InterpreterState(new SerializableDictionary<string, InterpreterState>());

        //file.Close();

        FileStream file = File.Open(Application.persistentDataPath + "/" + SAVE_PREFIX + saveName + ".dat", FileMode.OpenOrCreate);

        curState.LineNum--;
        bf.Serialize(file, curState);
        file.Close();
    }

    public override void Load(string saveName = "")
    {
        if (File.Exists(Application.persistentDataPath + "/" + SAVE_PREFIX + saveName + ".dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/" + SAVE_PREFIX + saveName + ".dat", FileMode.Open);

            // InterpreterState curState;
            if (file.Length > 0) // Checks for empty save files
                curState = bf.Deserialize(file) as InterpreterState;
            else
            {
                curState = new InterpreterState(0, curState.BoolDict, curState.StringDict, curState.FloatDict);
            }
            file.Close();

            try // Used to check if file exists, but no data for current scene exists
            {
                if (curState == null) { }
            }
            catch (KeyNotFoundException)
            {
                this.curState = new InterpreterState(0, curState.BoolDict, curState.StringDict, curState.FloatDict);
                return;
            }


            //curState.LineNum = PrevLine(this.curState.LineNum);
        }
        else
        {
            Debug.Log("No save file exists");
            curState = new InterpreterState();
        }
    }

    [Serializable]
    public class InterpreterState
    {
        [SerializeField]
        public int LineNum = 0;
        [SerializeField]
        public SerializableDictionary<string, bool> BoolDict = new SerializableDictionary<string, bool>();
        [SerializeField]
        public SerializableDictionary<string, string> StringDict = new SerializableDictionary<string, string>();
        [SerializeField]
        public SerializableDictionary<string, float> FloatDict = new SerializableDictionary<string, float>();

        public InterpreterState() { } // Default constructor
        public InterpreterState(int lineNum,
            SerializableDictionary<string, bool> boolDict,
            SerializableDictionary<string, string> stringDict,
            SerializableDictionary<string, float> floatDict)
        {
            LineNum = lineNum;
            BoolDict = boolDict;
            StringDict = stringDict;
            FloatDict = floatDict;
        }
    }

    //[Serializable]
    //private class BookmarkSave
    //{
    //    [SerializeField]
    //    public SerializableDictionary<string, InterpreterState> bookmarks;

    //    public BookmarkSave(SerializableDictionary<string, InterpreterState> data)
    //    {
    //        bookmarks = data;
    //    }
    //}

    #endregion

}
