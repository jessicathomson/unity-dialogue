﻿using UnityEngine;

public static class ScriptFunctions {

    /************************************************************************
     *                                                                      *
     *  • Functions in this file can be called from dialogue scripts with   *
     *    the following syntax:                                             *
     *                                                                      *
     *      run FunctionName param1 param2 param3                           *
     *                                                                      *
     *  • Valid parameter types are: float, bool, string (surrounded by "") *
     *  • ANY static function can be called from this script                *
     *  • Alternatively, any other static class can be used in the place    *
     *    of this script, if you don't need the example scripts             *
     *  • Overloaded versions of a script will be called, if possible       *
     *                                                                      *
     ************************************************************************/

    //** These three test functions show how to overload **//
    public static void TestType(float i)
    { Debug.Log(i + " is a float"); }
    public static void TestType(string i)
    { Debug.Log(i + " is a string"); }
    public static void TestType(bool i)
    { Debug.Log(i + " is a bool"); }

    public static void DisplayCube(float i)
    {
        GameObject.Find("Cube").
            GetComponent<MeshRenderer>().enabled = true;
    }

    public static void DoThing(bool thing1, string thing2, float thing3)
    {
        Debug.Log(thing1 + ", " + thing2 + ", " + thing3);
    }

    public static void LerpPose(string target, string destination)
    {
        //(Dialogue.Current as SureDialogue).LerpPose(target, destination);
    }

}
