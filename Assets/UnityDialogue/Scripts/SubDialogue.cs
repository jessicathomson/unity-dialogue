﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubDialogue : Dialogue
{
    public void TestType(float i)
    { Debug.Log(i + " is a float"); }
    public void TestType(string i)
    { Debug.Log(i + " is a string"); }
    public void TestType(bool i)
    { Debug.Log(i + " is a bool"); }

    public void DisplayCube(float i)
    {
        GameObject.Find("Cube").
            GetComponent<MeshRenderer>().enabled = true;
    }

    public void DoThing(bool thing1, string thing2, float thing3)
    {
        Debug.Log(thing1 + ", " + thing2 + ", " + thing3);
    }

    public void LerpPose(string target, string destination)
    {
        //(Dialogue.Current as SureDialogue).LerpPose(target, destination);
    }
}
